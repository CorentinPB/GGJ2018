#include "GameMenu.h"

void GameMenu::init() {
    Sprite* s = new Sprite(1600/2-(298/2),900/2-(335/2),"sprites/LevelFinished.png");
    m_sprites.push_back(s);
    Button* b = new Button(1600/2-(226/2),900/2-(100/2)-30,"sprites/MainMenuButton.png","sprites/MainMenuButtonPressed.png");
    m_buttons.push_back(b);
    b = new Button(1600/2-(226/2),900/2-(100/2)+80,"sprites/NextLevelButton.png","sprites/NextLevelButtonPressed.png");
    m_buttons.push_back(b);
}
