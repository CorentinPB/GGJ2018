#ifndef SELECTMENU_H
#define SELECTMENU_H

#include "Menu.h"
#include <map>

class SelectMenu : public Menu
{
public:
    init();
    ~SelectMenu();
    std::map<int,float> m_scores;
    sf::Text m_text;
    sf::Font m_font;
    void draw_to(sf::RenderWindow &window);
};

#endif // SELECTMENU_H
