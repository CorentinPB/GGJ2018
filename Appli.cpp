#include "Appli.h"

const float RATIO = 1;

using namespace std;

Appli::Appli():
    m_cursor{sf::Vector2f{20,20}},
    m_place{Place::MAIN_MENU},
    level{&selectMenu}
{
    mainMenu.init();
    selectMenu.init();
    m_font.loadFromFile("PoiretOne-Regular.ttf");
    m_time.setFont(m_font);
    m_time.setCharacterSize(30);
    m_time.setOrigin(sf::Vector2f(m_time.getLocalBounds().width/2,m_time.getLocalBounds().height/2));
    m_time.setPosition(sf::Vector2f{800,50});
    musicGame.setPitch(1);
    musicGame.setVolume(50);
    musicGame.setLoop(true);
    m_cursor.setFillColor(sf::Color::White);
    m_cursor.setOrigin(sf::Vector2f(m_cursor.getLocalBounds().width/2,m_cursor.getLocalBounds().height/2));
    gameMenu.init();
}

Appli::~Appli() {

}

void Appli::update() {
    switch (m_place) {
    case Place::MAIN_MENU:
        mainMenu.update();
    case Place::SELECT_MENU:
        m_cursor.setPosition(m_mouse);
        particles.push_back(Gaz(m_cursor.getPosition()));
        for(Gaz g : particles)
            g.update();
        for( int i = particles.size()-1 ; i >= 0 ; --i ) {
            particles[i].update();
            if(particles[i].isDead())
                particles.erase(particles.begin()+i);
        }
        break;
    case Place::IN_GAME:
        if(!level.isWin()){
            m_cursor.setPosition( level.toScreen(m_mousePos) );
            m_time.setString(level.getTime());
            level.updateChips();
            level.update(m_mousePos);
        }else{
            m_cursor.setPosition(m_mouse);
        }
        break;
    case Place::END_MENU:
        break;
    case Place::CREDITS:
        m_cursor.setPosition(m_mouse);
        particles.push_back(Gaz(m_cursor.getPosition()));
        for(Gaz g : particles)
            g.update();
        for( int i = particles.size()-1 ; i >= 0 ; --i ) {
            particles[i].update();
            if(particles[i].isDead())
                particles.erase(particles.begin()+i);
        }
        break;
    default:
        break;
    }
}

void Appli::run() {
    m_window.create(sf::VideoMode{ WIDTH,HEIGHT,32 },
                    "GGJ2018",
                    sf::Style::Close
                    );

    m_window.setFramerateLimit(144);
    m_window.setMouseCursorVisible(false);
    m_appli_running = true;
    while (m_appli_running) {
        process_events();
        update();
        draw();
    }
}

void Appli::draw() {
    m_window.clear(sf::Color(50,50,65));

    switch (m_place) {
    case Place::MAIN_MENU:
        mainMenu.draw_to(m_window);
        for(Gaz g : particles)
            g.render(m_window);
        break;
    case Place::IN_GAME:
        if(level.render(m_window,m_mousePos,m_cursor.getGlobalBounds())) {
            m_mousePos = level.getStartPos();
        }
        m_window.draw(m_time);
        if(level.isWin())
            gameMenu.draw_to(m_window);
        break;
    case Place::SELECT_MENU:
        selectMenu.draw_to(m_window);
        for(Gaz g : particles)
            g.render(m_window);
        break;
    case Place::END_MENU:
        break;
    case Place::CREDITS:
        creditmenu.draw_to(m_window);
        for(Gaz g : particles)
            g.render(m_window);
        break;
    default:
        break;
    }
    m_window.draw(m_cursor);
    m_window.display();
}

void Appli::keyPressed(sf::Keyboard::Key code) {
    switch (code) {
    case sf::Keyboard::Escape:
        switch (m_place) {
        case Place::MAIN_MENU:
            m_appli_running = false;
            break;
        case Place::IN_GAME:
            m_place = Place::SELECT_MENU;
            sf::Mouse::setPosition(old_mousePos,m_window);
            m_mouse = sf::Vector2f(old_mousePos);
            break;
        case Place::SELECT_MENU:
            m_place = Place::MAIN_MENU;
            break;
        case Place::CREDITS:
            m_place = Place::MAIN_MENU;
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}

void Appli::KeyReleased(sf::Keyboard::Key code) {
    switch (code) {
    default:
        break;
    }
}

void Appli::mousePressed() {
    switch (m_place) {
    case Place::MAIN_MENU:
        m_selected_button = mainMenu.buttonContains(m_mouse);
        m_selected_button_int = mainMenu.buttonContainsIndice(m_mouse);
        if(m_selected_button != nullptr) {
            m_selected_button->setPressed(true);
        }
        break;
    case Place::SELECT_MENU:
        m_selected_button = selectMenu.buttonContains(m_mouse);
        m_selected_button_int = selectMenu.buttonContainsIndice(m_mouse);
        if(m_selected_button != nullptr) {
            m_selected_button->setPressed(true);
        }
        break;
    case Place::IN_GAME:
        if(level.isWin()){
            m_selected_button = gameMenu.buttonContains(m_mouse);
            m_selected_button_int = gameMenu.buttonContainsIndice(m_mouse);
            if(m_selected_button != nullptr) {
                m_selected_button->setPressed(true);
            }
        }
        break;
    case Place::CREDITS:
        m_selected_button = creditmenu.buttonContains(m_mouse);
        m_selected_button_int = creditmenu.buttonContainsIndice(m_mouse);
        if(m_selected_button != nullptr) {
            m_selected_button->setPressed(true);
        }
        break;
    default:
        break;
    }
}

void Appli::mouseReleased() {
    switch (m_place) {
    case Place::MAIN_MENU:
        if(m_selected_button != nullptr){
            m_selected_button->setPressed(false);
            if(mainMenu.containsIndice(m_selected_button_int,m_mouse)){
                switch (m_selected_button_int) {
                case 0:
                    m_place = Place::SELECT_MENU;
                    break;
                case 1:
                    m_appli_running = false;
                    break;
                case 2:
                    m_place = Place::CREDITS;
                    break;
                default:
                    break;
                }
            }
            m_selected_button = nullptr;
        }
        break;
    case Place::IN_GAME:
        if(m_selected_button != nullptr){
            m_selected_button->setPressed(false);
            if(gameMenu.containsIndice(m_selected_button_int,m_mouse)){
                switch (m_selected_button_int) {
                case 0:
                    m_place = Place::SELECT_MENU;
                    break;
                case 1:
                    if(level.nbLvl < 10){
                        level.init(level.nbLvl+1);
                        m_mousePos = level.getStartPos();
                        sf::Mouse::setPosition(sf::Vector2i{800,450}, m_window);
                    }
                    break;
                default:
                    break;
                }
            }
            m_selected_button = nullptr;
        }
        break;
    case Place::CREDITS:
        if(m_selected_button != nullptr){
            m_selected_button->setPressed(false);
            if(creditmenu.containsIndice(m_selected_button_int,m_mouse)){
                switch (m_selected_button_int) {
                case 0:
                    m_place = Place::MAIN_MENU;
                    break;
                default:
                    break;
                }
            }
            m_selected_button = nullptr;
        }
        break;
    case Place::SELECT_MENU:
        if(m_selected_button != nullptr){
            m_selected_button->setPressed(false);
            if(selectMenu.containsIndice(m_selected_button_int,m_mouse)){
                old_mousePos = sf::Mouse::getPosition(m_window);
                switch (m_selected_button_int) {
                case 0:
                    m_place = Place::MAIN_MENU;
                    break;
                case 1:
                    level.init(1);
                    break;
                case 2:
                    level.init(2);
                    break;
                case 3:
                    level.init(3);
                    break;
                case 4:
                    level.init(4);
                    break;
                case 5:
                    level.init(5);
                    break;
                case 6:
                    level.init(6);
                    break;
                case 7:
                    level.init(7);
                    break;
                case 8:
                    level.init(8);
                    break;
                case 9:
                    level.init(9);
                    break;
                case 10:
                    level.init(10);
                    break;
                default:
                    break;
                }
                if(m_selected_button_int != 0){
                    m_mousePos = level.getStartPos();
                    m_place = Place::IN_GAME;
                    particles.clear();
                    sf::Mouse::setPosition(sf::Vector2i{800,450}, m_window);
                }
            }
            m_selected_button = nullptr;
        }
        break;
    case Place::END_MENU:
        break;
    default:
        break;
    }
}

void Appli::mouseMoved() {
    switch (m_place) {
    case Place::MAIN_MENU:
        break;
    case Place::IN_GAME:
        break;
    case Place::SELECT_MENU:
        break;
    case Place::END_MENU:
        break;
    default:
        break;
    }
}

void Appli::process_events()
{

    sf::Vector2f movement;
    sf::Event event;
    while (m_window.pollEvent(event)) {
        switch (event.type) {
        case sf::Event::Closed:
            m_appli_running = false;
            break;
        case sf::Event::KeyPressed:
            keyPressed(event.key.code);
            break;
        case sf::Event::KeyReleased:
            KeyReleased(event.key.code);
            break;
        case sf::Event::MouseButtonPressed:
            //m_button = event.mouseButton.button;
            mousePressed();
            break;
        case sf::Event::MouseButtonReleased:
            //m_button = event.mouseButton.button;
            mouseReleased();
            break;
        case sf::Event::MouseMoved:
            switch (m_place) {
            case Place::IN_GAME:
                if(!level.isWin()){
                    movement = m_window.mapPixelToCoords({ event.mouseMove.x,
                                                           event.mouseMove.y }) - sf::Vector2f{800,450};
                    movement.x *= RATIO; movement.y *= RATIO*1.3;
                    m_mousePos.x = constraint(m_mousePos.x+movement.x,0,900000);
                    m_mousePos.y = constraint(m_mousePos.y+movement.y,0,900000);
                    sf::Mouse::setPosition(sf::Vector2i{800,450}, m_window);
                }else{
                    m_mouse = m_window.mapPixelToCoords({ event.mouseMove.x,
                                                          event.mouseMove.y });
                }
                break;
            case Place::MAIN_MENU:
            case Place::SELECT_MENU:
            case Place::CREDITS:
                m_mouse = m_window.mapPixelToCoords({ event.mouseMove.x,
                                                      event.mouseMove.y });
                break;
            case Place::END_MENU:
                break;
            default:
                break;
            }
            mouseMoved();
            break;
        case sf::Event::MouseWheelMoved:
            if(m_place == Place::IN_GAME){
            }
        default:
            break;
        }
    }
}
