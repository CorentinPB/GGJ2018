#ifndef SPRITE_H
#define SPRITE_H

#include "SFML/Graphics.hpp"

class Sprite {

public:
    Sprite(const std::string &file);
    Sprite(const int &x, const int &y, const std::string &file);
    Sprite(const int &x, const int &y, const std::string &file, const std::string &text);

    sf::Vector2i    getSize()       const;
    sf::FloatRect getLocalBounds()       const;
    sf::Vector2f    getPosition()       const;

    void draw_to(sf::RenderWindow &window);
private:
    bool m_with_text = 0;

    std::string m_fileName;
    sf::Texture m_image;
    sf::Sprite  m_sprite;
    sf::Font    m_font;
    sf::Text    m_text;
};

#endif // SPRITE_H
