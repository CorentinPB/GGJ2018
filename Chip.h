#ifndef CHIP_H
#define CHIP_H

#include <vector>
#include <SFML/Graphics.hpp>
#include <iostream>

class Chip
{
public:
    Chip(const sf::Vector2f &pos, bool m_type );
    void update();
    void render(sf::RenderWindow &w, const sf::FloatRect &screen);
    sf::Vector2f
    applyRepulsion(const sf::Vector2f &pos);
    sf::Sprite chip;
private:
    std::vector<sf::CircleShape> waves;
    sf::Texture texture;
    bool type = false;
};

#endif // CHIP_H
