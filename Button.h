#ifndef BUTTON_H
#define BUTTON_H

#include <string>
#include <vector>
#include "Sprite.h"
#include <SFML/Graphics.hpp>

const int INSTRUCTION_SIZE = 40;

class Button
{
private:
    Sprite* m_sprite;
    Sprite* m_spritePressed;

    bool m_isPressed;
public:
    // Contructors
    Button(const std::string &file);
    Button(const std::string &file, const std::string &filePressed);
    Button(const int &x, const int &y, const std::string &file);
    Button(const int &x, const int &y, const std::string &file, int number);
    Button(const int &x, const int &y, const std::string &file, const std::string &filePressed);
    Button(const int &x, const int &y, const std::string &file, const std::string &filePressed, const std::string &text);

    bool contains(const sf::Vector2f &vect)         const;

    sf::Vector2f getPosition();
    void setPressed(bool);

    void draw_to(sf::RenderWindow &window);

private:
};

#endif // BUTTON_H
