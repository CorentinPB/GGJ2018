#include "Chip.h"

using namespace std;

Chip::Chip(const sf::Vector2f &pos, bool m_type)
    :type{m_type} {
    texture.loadFromFile("sprites/PC.png");
    texture.setSmooth(1);
    chip.setTexture(texture);
    chip.setScale(0.3,0.3);
    chip.setPosition(pos);
    chip.setOrigin({ chip.getLocalBounds().width/2
                     , chip.getLocalBounds().height/2});
    for(int i = 0; i < 5; ++i) {
        sf::CircleShape circ;
        circ.setPosition(pos);
        circ.setRadius( i*50 );
        circ.setFillColor(sf::Color::Transparent);
        circ.setOutlineColor(sf::Color::White);
        circ.setOutlineThickness(3);
        circ.setOrigin((i*20 + 50)/2,(i*20 + 50)/2);
        waves.push_back(circ);
    }
}

void Chip::update() {
    for(sf::CircleShape &c : waves) {
        c.setRadius(c.getRadius()+1);
        if(c.getRadius() > 250)
            c.setRadius(0);
        c.setOutlineThickness(8.0-(2.0*c.getRadius()/50.0)); c.setOutlineColor(sf::Color(255,255,255,155-(c.getRadius()*100/255)));
        c.setOrigin(c.getRadius(),c.getRadius());
    }
}

void Chip::render(sf::RenderWindow &w, const sf::FloatRect &screen) {
    if(screen.intersects(chip.getGlobalBounds())) {
        for(sf::CircleShape c : waves) {
            c.setPosition(c.getPosition().x - screen.left, c.getPosition().y - screen.top);
            w.draw(c);
        }
        sf::Vector2f old = chip.getPosition();
        chip.setPosition(chip.getPosition().x - screen.left, chip.getPosition().y - screen.top);
        w.draw(chip);
        chip.setPosition(old);
    }
}

sf::Vector2f Chip::applyRepulsion(const sf::Vector2f &pos) {
    sf::Vector2f diff;
    diff = pos-chip.getPosition();
    if(!type)
        return sf::Vector2f{diff.x/90,diff.y/90};
    return sf::Vector2f{-diff.x/90,-diff.y/90};
}
