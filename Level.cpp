#include <iomanip> // setprecision
#include <sstream> // stringstream

#include "Level.h"

using namespace std;

float constraint(float val, float min, float max) {
    if(val > max) return max;
    if(val < min) return min;
    return val;
}

Level::Level(SelectMenu* sm)
    :nbLvl{1}
{
    selectmenu = sm;
    for(int i = 0 ; i < TailleXGrille ; ++i)
        for(int j = 0 ; j < TailleYGrille ; ++j)
            grille[i][j] = 0;
    textureEngrenage.loadFromFile("sprites/gears.png");
    textureEngrenage.setSmooth(1);
}

void Level::updateChips() {
    for(Chip* c : chips)
        c->update();
    for(pair<sf::Sprite,float> &s : engrenages) {
        s.first.setRotation(s.first.getRotation() + s.second);
    }
}

void Level::update(sf::Vector2f &mouse) {
    for(Chip* c : chips)
        if(sqrt(pow(c->chip.getPosition().x-mouse.x,2)+pow(c->chip.getPosition().y-mouse.y,2)) < 250)
            mouse += c->applyRepulsion(mouse);
}

std::string Level::getTime(){
    if(!m_isWin){
        stringstream stream;
        stream << fixed << setprecision(2) << m_clock.getElapsedTime().asSeconds();
        return stream.str();
    }else{
        stringstream stream;
        stream << fixed << setprecision(2) << endTime;
        return stream.str();
    }
}

float Level::getTimeFloat(){
    if(!m_isWin){
        return m_clock.getElapsedTime().asSeconds();
    }else{
        return endTime;
    }
}

void Level::init(int nb) {

    sf::RectangleShape rect;
    sf::Sprite gear;

    for(int i = 0 ; i < TailleXGrille ; ++i)
        for(int j = 0 ; j < TailleYGrille ; ++j)
            grille[i][j] = 0;

    rectangles.clear();
    chips.clear();
    nbLvl = nb;
    rectangles.clear();
    engrenages.clear();
    switch (nb) {
    case 1:
        rect.setSize({150,100});
        rect.setFillColor(sf::Color::Black);
        rect.setPosition({260,800});
        rectangles.push_back(rect);
        rect.setSize({250,100});
        rect.setPosition({900,900});
        rectangles.push_back(rect);
        rect.setSize({100,200});
        rect.setPosition({1200,1200});
        rectangles.push_back(rect);
        rect.setSize({50,300});
        rect.setPosition({2850,900});
        rectangles.push_back(rect);
        rect.setSize({20,100});
        rect.setPosition({4150,950});
        rectangles.push_back(rect);
        rect.setSize({20,100});
        rect.setPosition({4000,1150});
        rectangles.push_back(rect);

        gear.setPosition({200,600});
        gear.setTexture(textureEngrenage);
        gear.setOrigin({gear.getLocalBounds().width/2, gear.getLocalBounds().height/2});
        gear.setRotation(rand()%360);
        //engrenages.push_back(std::pair<sf::Sprite,float>(gear,((rand()%20)-10)/10.0));

        realX = 26;
        realY = 9;
        startPosition = {0,2};
        grille[0][2] = 1;
        grille[1][2] = 1;
        grille[1][3] = 1;
        grille[1][4] = 1;
        grille[1][5] = 1;
        grille[2][5] = 1;
        grille[3][5] = 1;
        grille[4][5] = 1;
        grille[4][4] = 1;
        grille[5][4] = 1;
        grille[6][4] = 1;
        grille[6][5] = 1;
        grille[6][6] = 1;
        grille[7][6] = 1;
        grille[8][6] = 1;
        grille[9][6] = 1;
        grille[10][6] = 1;
        grille[11][6] = 1;
        grille[11][7] = 1;
        grille[12][7] = 1;
        grille[13][7] = 1;
        grille[14][7] = 1;
        grille[14][6] = 1;
        grille[14][5] = 1;
        grille[14][4] = 1;
        grille[14][3] = 1;
        grille[15][3] = 1;
        grille[16][3] = 1;
        grille[16][4] = 1;
        grille[17][4] = 1;
        grille[18][4] = 1;
        grille[18][5] = 1;
        grille[19][5] = 1;
        grille[20][5] = 1;
        grille[21][5] = 1;
        grille[21][4] = 1;
        grille[22][4] = 1;
        grille[23][4] = 1;
        grille[23][5] = 1;
        grille[24][5] = 1;
        grille[25][5] = 1;
        endPosition = {25,5};
        break;
    case 2:

        rect.setSize({300,150});
        rect.setFillColor(sf::Color::Black);
        rect.setPosition({1200,850});
        rectangles.push_back(rect);
        rect.setSize({70,50});
        rect.setPosition({1300,1800});
        rectangles.push_back(rect);
        rect.setSize({140,290});
        rect.setPosition({2500,1300});
        rectangles.push_back(rect);
        rect.setSize({100,120});
        rect.setPosition({3200,1000});
        rectangles.push_back(rect);

        realX = 24;
        realY = 11;
        startPosition = {0,2};
        grille[0][2] = 1;
        grille[1][2] = 1;
        grille[2][2] = 1;
        grille[3][2] = 1;
        grille[4][2] = 1;
        grille[4][3] = 1;
        grille[5][3] = 1;
        grille[5][4] = 1;
        grille[6][4] = 1;
        grille[7][4] = 1;
        grille[8][4] = 1;
        grille[9][4] = 1;
        grille[10][4] = 1;
        grille[10][5] = 1;
        grille[10][6] = 1;
        grille[8][5] = 1;
        grille[8][6] = 1;
        grille[7][6] = 1;
        grille[6][6] = 1;
        grille[6][7] = 1;
        grille[6][8] = 1;
        grille[6][9] = 1;
        grille[7][9] = 1;
        grille[8][9] = 1;
        grille[9][9] = 1;
        grille[10][9] = 1;
        grille[11][9] = 1;
        grille[11][8] = 1;
        grille[11][7] = 1;
        grille[12][7] = 1;
        grille[12][6] = 1;
        grille[13][6] = 1;
        grille[14][6] = 1;
        grille[15][6] = 1;
        grille[16][6] = 1;
        grille[16][5] = 1;
        grille[17][5] = 1;
        grille[18][5] = 1;
        grille[18][6] = 1;
        grille[19][6] = 1;
        grille[20][6] = 1;
        grille[20][5] = 1;
        grille[21][5] = 1;
        grille[22][5] = 1;
        grille[23][5] = 1;
        endPosition = {23,5};
        break;
    case 3:

        rect.setSize({30,80});
        rect.setFillColor(sf::Color::Black);
        rect.setPosition({750,600});
        rectangles.push_back(rect);
        rect.setSize({30,80});
        rect.setPosition({700,720});
        rectangles.push_back(rect);
        rect.setSize({30,80});
        rect.setPosition({800,600});
        rectangles.push_back(rect);
        rect.setSize({30,80});
        rect.setPosition({850,720});
        rectangles.push_back(rect);
        rect.setSize({40,40});
        rect.setPosition({680,1070});
        rectangles.push_back(rect);
        rect.setSize({40,40});
        rect.setPosition({800,1050});
        rectangles.push_back(rect);
        rect.setSize({40,40});
        rect.setPosition({900,1100});
        rectangles.push_back(rect);
        rect.setSize({40,40});
        rect.setPosition({750,1010});
        rectangles.push_back(rect);
        rect.setSize({40,40});
        rect.setPosition({870,1150});
        rectangles.push_back(rect);
        rect.setSize({40,40});
        rect.setPosition({790,1130});
        rectangles.push_back(rect);
        rect.setSize({200,150});
        rect.setPosition({2400,600});
        rectangles.push_back(rect);
        rect.setSize({100,400});
        rect.setPosition({2400,1050});
        rectangles.push_back(rect);
        rect.setSize({100,400});
        rect.setPosition({2100,1050});
        rectangles.push_back(rect);
        rect.setSize({200,150});
        rect.setPosition({2100,600});
        rectangles.push_back(rect);
        rect.setSize({100,400});
        rect.setPosition({1800,1050});
        rectangles.push_back(rect);
        rect.setSize({200,150});
        rect.setPosition({1800,600});
        rectangles.push_back(rect);
        rect.setSize({250,130});
        rect.setPosition({3000,1050});
        rectangles.push_back(rect);
        rect.setSize({140,150});
        rect.setPosition({4000,500});
        rectangles.push_back(rect);
        rect.setSize({210,60});
        rect.setPosition({3700,900});
        rectangles.push_back(rect);
        rect.setSize({180,20});
        rect.setPosition({4480,750});
        rectangles.push_back(rect);

        realX = 25;
        realY = 8;
        startPosition = {0,4};
        grille[0][4] = 1;
        grille[1][4] = 1;
        grille[2][3] = 1;
        grille[2][4] = 1;
        grille[2][5] = 1;
        grille[3][3] = 1;
        grille[3][5] = 1;
        grille[4][3] = 1;
        grille[4][5] = 1;
        grille[5][3] = 1;
        grille[5][4] = 1;
        grille[5][5] = 1;
        grille[6][4] = 1;
        grille[7][4] = 1;
        grille[8][3] = 1;
        grille[8][4] = 1;
        grille[8][5] = 1;
        grille[9][3] = 1;
        grille[9][5] = 1;
        grille[10][3] = 1;
        grille[10][5] = 1;
        grille[11][3] = 1;
        grille[11][5] = 1;
        grille[12][3] = 1;
        grille[12][4] = 1;
        grille[12][5] = 1;
        grille[13][4] = 1;
        grille[14][4] = 1;
        grille[15][4] = 1;
        grille[15][3] = 1;
        grille[16][3] = 1;
        grille[17][3] = 1;
        grille[17][2] = 1;
        grille[18][2] = 1;
        grille[19][2] = 1;
        grille[20][2] = 1;
        grille[20][3] = 1;
        grille[21][3] = 1;
        grille[22][3] = 1;
        grille[22][4] = 1;
        grille[23][4] = 1;
        grille[24][4] = 1;
        grille[15][5] = 1;
        grille[16][5] = 1;
        grille[17][5] = 1;
        grille[17][6] = 1;
        grille[18][6] = 1;
        grille[19][6] = 1;
        grille[20][6] = 1;
        grille[20][5] = 1;
        grille[21][5] = 1;
        grille[22][5] = 1;
        endPosition = {24,4};
        break;
    case 4:

        rect.setSize({30,80});
        rect.setFillColor(sf::Color::Black);
        rect.setPosition({750,1200});
        rectangles.push_back(rect);
        rect.setPosition({1200,1400});
        rect.setSize({50,150});
        rectangles.push_back(rect);
        rect.setPosition({2200,1450});
        rect.setSize({50,150});
        rectangles.push_back(rect);

        realX = 20;
        realY = 15;
        startPosition = {0,6};
        grille[0][6] = 1;
        grille[1][6] = 1;
        grille[2][6] = 1;
        grille[2][5] = 1;
        grille[2][4] = 1;
        grille[3][4] = 1;
        grille[3][3] = 1;
        grille[3][2] = 1;
        grille[4][2] = 1;
        grille[5][2] = 1;
        grille[6][2] = 1;
        grille[6][3] = 1;
        grille[7][3] = 1;
        grille[8][3] = 1;
        grille[8][4] = 1;
        grille[8][5] = 1;
        grille[9][5] = 1;
        grille[10][5] = 1;
        grille[11][5] = 1;
        grille[12][5] = 1;
        grille[12][4] = 1;
        grille[12][3] = 1;
        grille[13][3] = 1;
        grille[14][3] = 1;
        grille[15][3] = 1;
        grille[15][4] = 1;
        grille[16][4] = 1;
        grille[17][4] = 1;
        grille[17][5] = 1;
        grille[17][6] = 1;
        grille[17][7] = 1;
        grille[18][7] = 1;
        grille[19][7] = 1;
        grille[3][6] = 1;
        grille[4][6] = 1;
        grille[5][6] = 1;
        grille[5][7] = 1;
        grille[6][7] = 1;
        grille[7][7] = 1;
        grille[8][7] = 1;
        grille[9][7] = 1;
        grille[10][7] = 1;
        grille[11][7] = 1;
        grille[12][7] = 1;
        grille[13][7] = 1;
        grille[14][7] = 1;
        grille[15][7] = 1;
        grille[16][7] = 1;
        grille[2][7] = 1;
        grille[2][8] = 1;
        grille[2][9] = 1;
        grille[3][9] = 1;
        grille[3][10] = 1;
        grille[4][10] = 1;
        grille[5][10] = 1;
        grille[5][11] = 1;
        grille[5][12] = 1;
        grille[5][13] = 1;
        grille[6][13] = 1;
        grille[7][13] = 1;
        grille[8][13] = 1;
        grille[8][12] = 1;
        grille[9][12] = 1;
        grille[9][11] = 1;
        grille[9][10] = 1;
        grille[9][9] = 1;
        grille[10][9] = 1;
        grille[11][9] = 1;
        grille[12][9] = 1;
        grille[12][10] = 1;
        grille[13][10] = 1;
        grille[13][11] = 1;
        grille[13][12] = 1;
        grille[14][12] = 1;
        grille[15][12] = 1;
        grille[15][11] = 1;
        grille[16][11] = 1;
        grille[16][10] = 1;
        grille[16][9] = 1;
        grille[17][9] = 1;
        grille[17][8] = 1;
        endPosition = {19,7};
        chips.push_back(new Chip({LEFT_PART+4*TailleCase+TailleCase/2, 7*TailleCase+TailleCase/2},false));
        chips.push_back(new Chip({LEFT_PART+11*TailleCase+TailleCase/2, 6*TailleCase+TailleCase/2},false));
        chips.push_back(new Chip({LEFT_PART+16*TailleCase+TailleCase/2, 8*TailleCase+TailleCase/2},false));
        break;
    case 5:

        rect.setSize({30,120});
        rect.setFillColor(sf::Color::Black);
        rect.setPosition({1300,1200});
        rectangles.push_back(rect);
        rect.setSize({30,200});
        rect.setPosition({1600,1200});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({950,1400});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({600,1400});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({1400,1970});
        rectangles.push_back(rect);
        rect.setSize({30,200});
        rect.setPosition({1600,1600});
        rectangles.push_back(rect);
        rect.setSize({30,200});
        rect.setPosition({2000,1200});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({1800,1970});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({2200,1970});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({2200,1570});
        rectangles.push_back(rect);
        rect.setSize({30,200});
        rect.setPosition({2400,1200});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({2600,1570});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({2200,600});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({2200,1000});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({1800,600});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({2600,1000});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({1350,600});
        rectangles.push_back(rect);
        rect.setSize({250,30});
        rect.setPosition({1350,1000});
        rectangles.push_back(rect);
        rect.setSize({30,250});
        rect.setPosition({1300,800});
        rectangles.push_back(rect);

        realX = 16;
        realY = 14;
        startPosition = {0,6};
        grille[0][6] = 1;
        grille[1][6] = 1;
        grille[2][6] = 1;
        grille[3][2] = 1;
        grille[3][3] = 1;
        grille[3][4] = 1;
        grille[3][5] = 1;
        grille[3][6] = 1;
        grille[3][7] = 1;
        grille[3][8] = 1;
        grille[3][9] = 1;
        grille[3][10] = 1;
        grille[3][11] = 1;
        grille[3][12] = 1;
        grille[4][2] = 1;
        grille[4][4] = 1;
        grille[4][6] = 1;
        grille[4][8] = 1;
        grille[4][10] = 1;
        grille[4][12] = 1;
        grille[5][2] = 1;
        grille[5][3] = 1;
        grille[5][4] = 1;
        grille[5][5] = 1;
        grille[5][6] = 1;
        grille[5][7] = 1;
        grille[5][8] = 1;
        grille[5][9] = 1;
        grille[5][10] = 1;
        grille[5][11] = 1;
        grille[5][12] = 1;
        grille[6][2] = 1;
        grille[6][4] = 1;
        grille[6][6] = 1;
        grille[6][8] = 1;
        grille[6][10] = 1;
        grille[6][12] = 1;
        grille[7][2] = 1;
        grille[7][3] = 1;
        grille[7][4] = 1;
        grille[7][5] = 1;
        grille[7][6] = 1;
        grille[7][7] = 1;
        grille[7][8] = 1;
        grille[7][9] = 1;
        grille[7][10] = 1;
        grille[7][11] = 1;
        grille[7][12] = 1;
        grille[8][2] = 1;
        grille[8][4] = 1;
        grille[8][6] = 1;
        grille[8][8] = 1;
        grille[8][10] = 1;
        grille[8][12] = 1;
        grille[9][2] = 1;
        grille[9][3] = 1;
        grille[9][4] = 1;
        grille[9][5] = 1;
        grille[9][6] = 1;
        grille[9][7] = 1;
        grille[9][8] = 1;
        grille[9][9] = 1;
        grille[9][10] = 1;
        grille[9][11] = 1;
        grille[9][12] = 1;
        grille[10][2] = 1;
        grille[10][4] = 1;
        grille[10][6] = 1;
        grille[10][8] = 1;
        grille[10][10] = 1;
        grille[10][12] = 1;
        grille[11][2] = 1;
        grille[11][3] = 1;
        grille[11][4] = 1;
        grille[11][5] = 1;
        grille[11][6] = 1;
        grille[11][7] = 1;
        grille[11][8] = 1;
        grille[11][9] = 1;
        grille[11][10] = 1;
        grille[11][11] = 1;
        grille[11][12] = 1;
        grille[12][2] = 1;
        grille[12][4] = 1;
        grille[12][6] = 1;
        grille[12][8] = 1;
        grille[12][10] = 1;
        grille[12][12] = 1;
        grille[13][2] = 1;
        grille[13][3] = 1;
        grille[13][4] = 1;
        grille[13][5] = 1;
        grille[13][6] = 1;
        grille[13][7] = 1;
        grille[13][8] = 1;
        grille[13][9] = 1;
        grille[13][10] = 1;
        grille[13][11] = 1;
        grille[13][12] = 1;
        grille[14][10] = 1;
        grille[15][10] = 1;
        endPosition = {15,10};
        chips.push_back(new Chip({LEFT_PART+4*TailleCase+TailleCase/2, 3*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+8*TailleCase+TailleCase/2, 3*TailleCase+TailleCase/2},false));
        chips.push_back(new Chip({LEFT_PART+6*TailleCase+TailleCase/2, 7*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+10*TailleCase+TailleCase/2, 7*TailleCase+TailleCase/2},false));
        chips.push_back(new Chip({LEFT_PART+4*TailleCase+TailleCase/2, 9*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+8*TailleCase+TailleCase/2, 11*TailleCase+TailleCase/2},false));
        chips.push_back(new Chip({LEFT_PART+12*TailleCase+TailleCase/2, 11*TailleCase+TailleCase/2},true));
        break;
    case 6:

        rect.setSize({100,150});
        rect.setFillColor(sf::Color::Black);
        rect.setPosition({800,500});
        rectangles.push_back(rect);
        rect.setSize({100,150});
        rect.setPosition({1200,500});
        rectangles.push_back(rect);
        rect.setSize({100,150});
        rect.setPosition({1600,500});
        rectangles.push_back(rect);
        rect.setSize({100,150});
        rect.setPosition({1000,350});
        rectangles.push_back(rect);
        rect.setSize({100,150});
        rect.setPosition({1400,350});
        rectangles.push_back(rect);
        rect.setSize({100,150});
        rect.setPosition({1800,350});
        rectangles.push_back(rect);
        rect.setSize({200,150});
        rect.setPosition({2200,490});
        rectangles.push_back(rect);
        rect.setSize({200,150});
        rect.setPosition({4000,350});
        rectangles.push_back(rect);

        realX = 26;
        realY = 5;
        startPosition = {0,2};
        for (int i=0; i<26;i++) {
            grille[i][2] = 1;
        }
        endPosition = {25,2};
        chips.push_back(new Chip({LEFT_PART+11*TailleCase+TailleCase/2, 1*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+20*TailleCase+TailleCase/2, 3*TailleCase+TailleCase/2},true));
        break;
    case 7:
        realX = 16;
        realY = 12;
        startPosition = {0,7};
        grille[0][7] = 1;
        grille[1][7] = 1;
        grille[1][6] = 1;
        grille[2][6] = 1;
        grille[3][6] = 1;
        grille[3][7] = 1;
        grille[3][8] = 1;
        grille[4][8] = 1;
        grille[5][8] = 1;
        grille[5][7] = 1;
        grille[5][6] = 1;
        grille[5][5] = 1;
        grille[5][4] = 1;
        grille[6][4] = 1;
        grille[7][4] = 1;
        grille[7][5] = 1;
        grille[7][6] = 1;
        grille[7][7] = 1;
        grille[7][8] = 1;
        grille[7][9] = 1;
        grille[8][9] = 1;
        grille[9][9] = 1;
        grille[9][8] = 1;
        grille[9][7] = 1;
        grille[9][6] = 1;
        grille[9][5] = 1;
        grille[9][4] = 1;
        grille[9][3] = 1;
        grille[10][3] = 1;
        grille[11][3] = 1;
        grille[11][4] = 1;
        grille[11][5] = 1;
        grille[11][6] = 1;
        grille[11][7] = 1;
        grille[11][8] = 1;
        grille[11][9] = 1;
        grille[11][10] = 1;
        grille[12][10] = 1;
        grille[13][10] = 1;
        grille[13][9] = 1;
        grille[13][8] = 1;
        grille[13][7] = 1;
        grille[13][6] = 1;
        grille[14][6] = 1;
        grille[15][6] = 1;
        endPosition = {15,6};
        chips.push_back(new Chip({LEFT_PART+4*TailleCase+TailleCase/2, 7*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+6*TailleCase+TailleCase/2, 5*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+8*TailleCase+TailleCase/2, 8*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+10*TailleCase+TailleCase/2, 4*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+12*TailleCase+TailleCase/2, 9*TailleCase+TailleCase/2},true));
        break;
    case 8:

        rect.setSize({80,150});
        rect.setFillColor(sf::Color::Black);
        rect.setPosition({800,500});
        rectangles.push_back(rect);
        rect.setSize({200,30});
        rect.setPosition({300,1180});
        rectangles.push_back(rect);
        rect.setSize({150,150});
        rect.setPosition({1400,850});
        rectangles.push_back(rect);
        rect.setSize({100,100});
        rect.setPosition({2400,1350});
        rectangles.push_back(rect);
        rect.setSize({200,20});
        rect.setPosition({3050,1800});
        rectangles.push_back(rect);
        rect.setSize({200,20});
        rect.setPosition({3050,2000});
        rectangles.push_back(rect);
        rect.setSize({200,20});
        rect.setPosition({3900,1800});
        rectangles.push_back(rect);
        rect.setSize({200,20});
        rect.setPosition({3900,2000});
        rectangles.push_back(rect);

        realX = 22;
        realY = 12;
        startPosition = {0,6};
        grille[0][6] = 1;
        grille[1][6] = 1;
        grille[1][5] = 1;
        grille[2][5] = 1;
        grille[2][4] = 1;
        grille[3][4] = 1;
        grille[3][3] = 1;
        grille[4][3] = 1;
        grille[4][2] = 1;
        grille[5][2] = 1;
        grille[6][2] = 1;
        grille[6][3] = 1;
        grille[7][3] = 1;
        grille[7][4] = 1;
        grille[8][4] = 1;
        grille[8][5] = 1;
        grille[9][5] = 1;
        grille[9][6] = 1;
        grille[10][6] = 1;
        grille[10][7] = 1;
        grille[11][7] = 1;
        grille[12][7] = 1;
        grille[12][8] = 1;
        grille[12][9] = 1;
        grille[13][9] = 1;
        grille[14][9] = 1;
        grille[15][9] = 1;
        grille[15][8] = 1;
        grille[15][10] = 1;
        grille[16][8] = 1;
        grille[16][10] = 1;
        grille[17][8] = 1;
        grille[17][10] = 1;
        grille[18][8] = 1;
        grille[18][10] = 1;
        grille[19][8] = 1;
        grille[19][9] = 1;
        grille[19][10] = 1;
        grille[20][9] = 1;
        grille[21][9] = 1;
        grille[22][9] = 1;
        endPosition = {21,9};
        chips.push_back(new Chip({LEFT_PART+1*TailleCase+TailleCase/2, 4*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+5*TailleCase+TailleCase/2, 3*TailleCase+TailleCase/2},false));
        chips.push_back(new Chip({LEFT_PART+8*TailleCase+TailleCase/2, 3*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+11*TailleCase+TailleCase/2, 8*TailleCase+TailleCase/2},false));
        break;
    case 9:

        realX = 19;
        realY = 12;
        startPosition = {0,6};
        grille[0][6] = 1;
        grille[1][6] = 1;
        grille[2][6] = 1;
        grille[2][5] = 1;
        grille[2][4] = 1;
        grille[2][7] = 1;
        grille[2][8] = 1;
        grille[3][3] = 1;
        grille[3][4] = 1;
        grille[3][8] = 1;
        grille[3][9] = 1;
        grille[4][2] = 1;
        grille[4][3] = 1;
        grille[4][9] = 1;
        grille[4][10] = 1;
        grille[5][2] = 1;
        grille[5][10] = 1;
        grille[6][2] = 1;
        grille[6][10] = 1;
        grille[7][2] = 1;
        grille[7][10] = 1;
        grille[7][3] = 1;
        grille[7][9] = 1;
        grille[8][3] = 1;
        grille[8][4] = 1;
        grille[8][9] = 1;
        grille[8][8] = 1;
        grille[9][8] = 1;
        grille[9][7] = 1;
        grille[9][4] = 1;
        grille[9][5] = 1;
        grille[9][6] = 1;
        grille[10][6] = 1;
        grille[11][6] = 1;
        grille[12][6] = 1;
        grille[13][6] = 1;
        grille[14][6] = 1;
        grille[15][6] = 1;
        grille[15][4] = 1;
        grille[15][5] = 1;
        grille[15][7] = 1;
        grille[15][8] = 1;
        grille[15][9] = 1;
        grille[15][9] = 1;
        grille[16][4] = 1;
        grille[16][9] = 1;
        grille[17][4] = 1;
        grille[17][5] = 1;
        grille[17][6] = 1;
        grille[17][7] = 1;
        grille[17][8] = 1;
        grille[17][9] = 1;
        grille[18][6] = 1;
        grille[19][6] = 1;
        endPosition = {18,6};
        chips.push_back(new Chip({LEFT_PART+3*TailleCase+TailleCase/2, 5*TailleCase+TailleCase/2},false));
        chips.push_back(new Chip({LEFT_PART+3*TailleCase+TailleCase/2, 7*TailleCase+TailleCase/2},false));
        chips.push_back(new Chip({LEFT_PART+6*TailleCase+TailleCase/2, 3*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+6*TailleCase+TailleCase/2, 9*TailleCase+TailleCase/2},false));
        chips.push_back(new Chip({LEFT_PART+15*TailleCase+TailleCase/2, 5*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+15*TailleCase+TailleCase/2, 8*TailleCase+TailleCase/2},true));
        break;
    case 10:
        realX = 25;
        realY = 17;
        startPosition = {0,10};
        grille[0][10] = 1;
        grille[1][10] = 1;
        grille[1][9] = 1;
        grille[2][9] = 1;
        grille[2][8] = 1;
        grille[3][8] = 1;
        grille[3][7] = 1;
        grille[4][7] = 1;
        grille[4][6] = 1;
        grille[4][5] = 1;
        grille[4][4] = 1;
        grille[5][4] = 1;
        grille[5][3] = 1;
        grille[5][2] = 1;
        grille[6][2] = 1;
        grille[7][2] = 1;
        grille[8][2] = 1;
        grille[9][2] = 1;
        grille[10][2] = 1;
        grille[10][3] = 1;
        grille[10][4] = 1;
        grille[11][4] = 1;
        grille[11][5] = 1;
        grille[11][6] = 1;
        grille[11][7] = 1;
        grille[11][8] = 1;
        grille[11][9] = 1;
        grille[10][9] = 1;
        grille[10][10] = 1;
        grille[9][10] = 1;
        grille[9][11] = 1;
        grille[8][11] = 1;
        grille[8][12] = 1;
        grille[7][12] = 1;
        grille[7][13] = 1;
        grille[7][14] = 1;
        grille[7][15] = 1;
        grille[8][15] = 1;
        grille[9][15] = 1;
        grille[9][14] = 1;
        grille[10][14] = 1;
        grille[11][14] = 1;
        grille[11][15] = 1;
        grille[12][15] = 1;
        grille[13][15] = 1;
        grille[13][14] = 1;
        grille[13][13] = 1;
        grille[13][12] = 1;
        grille[14][12] = 1;
        grille[15][12] = 1;
        grille[15][11] = 1;
        grille[15][10] = 1;
        grille[15][9] = 1;
        grille[15][8] = 1;
        grille[15][7] = 1;
        grille[15][6] = 1;
        grille[15][5] = 1;
        grille[16][5] = 1;
        grille[17][5] = 1;
        grille[17][6] = 1;
        grille[17][7] = 1;
        grille[18][7] = 1;
        grille[18][8] = 1;
        grille[19][8] = 1;
        grille[20][8] = 1;
        grille[21][8] = 1;
        grille[21][8] = 1;
        grille[22][8] = 1;
        grille[23][8] = 1;
        grille[24][8] = 1;
        endPosition = {24,8};
        chips.push_back(new Chip({LEFT_PART+4*TailleCase+TailleCase/2, 3*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+11*TailleCase+TailleCase/2, 10*TailleCase+TailleCase/2},false));
        chips.push_back(new Chip({LEFT_PART+10*TailleCase+TailleCase/2, 15*TailleCase+TailleCase/2},true));
        chips.push_back(new Chip({LEFT_PART+16*TailleCase+TailleCase/2, 6*TailleCase+TailleCase/2},false));
        chips.push_back(new Chip({LEFT_PART+20*TailleCase+TailleCase/2, 8*TailleCase+TailleCase/2},true));
        break;
    default:
        break;
    }
    m_isWin = false;
    m_clock.restart();
}

sf::Vector2f Level::getStartPos() {
    return sf::Vector2f{150, startPosition.y*TailleCase+TailleCase/2};
}

bool Level::isWin(){
    return m_isWin;
}

sf::Vector2f Level::toScreen(const sf::Vector2f &pos) {
    float x;
    if( pos.x < 800.0 ) x = pos.x;
    else if( pos.x >realX*TailleCase-800.0 ) x = 1600.0-abs(realX*TailleCase-pos.x);
    else x = 800.0;

    float y;
    if( pos.y < 450.0 ) y = pos.y;
    else if( pos.y > realY*TailleCase-450.0 ) y = 900.0-abs(realY*TailleCase-pos.y);
    else y = 450.0;

    return sf::Vector2f{x,y};
}

bool Level::render(sf::RenderWindow &w, const sf::Vector2f & mousePos, sf::FloatRect cursor) {
    bool touched = false;
    sf::RectangleShape actualCase;
    actualCase.setFillColor(sf::Color::Black);
    sf::Vector2f topLeft = mousePos - sf::Vector2f{800,450};
    topLeft.x = constraint(topLeft.x,0,realX*TailleCase-1600+LEFT_PART+RIGHT_PART);
    topLeft.y = constraint(topLeft.y,0,realY*TailleCase-900);
    sf::RectangleShape rect;
    rect.setPosition(topLeft);
    rect.setSize({1920,1080});
    for(int i = 0 ; i < realX ; ++i)
        for(int j = 0 ; j < realY ; ++j) {
            actualCase.setPosition(LEFT_PART+i*TailleCase,j*TailleCase);
            actualCase.setSize({TailleCase,TailleCase});
            if(rect.getLocalBounds().intersects(actualCase.getLocalBounds()) && grille[i][j] == 0) {
                actualCase.setPosition(actualCase.getPosition()-rect.getPosition());
                w.draw(actualCase);
                if(actualCase.getGlobalBounds().intersects(cursor) ){
                    touched = true;
                    m_clock.restart();
                }
            }else if(i == endPosition.x && j == endPosition.y){
                if(cursor.left+cursor.width+topLeft.x >= actualCase.getGlobalBounds().left+TailleCase/2){
                    if(!m_isWin){
                        endTime = m_clock.getElapsedTime().asSeconds();
                        m_isWin = true;
                        auto it = selectmenu->m_scores.find(nbLvl);
                        if (it != selectmenu->m_scores.end()){
                            if(selectmenu->m_scores.at(nbLvl) > endTime)
                                selectmenu->m_scores[nbLvl] = endTime;
                        }else{
                            selectmenu->m_scores.insert(std::pair<int,float>(nbLvl,endTime) );
                        }
                    }
                }
            }
        }
    for(sf::RectangleShape r : rectangles) {
        if(r.getGlobalBounds().intersects(rect.getGlobalBounds())) {
            sf::RectangleShape temp;
            temp = r;
            temp.setPosition(temp.getPosition()-topLeft);
            w.draw(temp);
            if(temp.getGlobalBounds().intersects(cursor)) {
                touched = true;
                m_clock.restart();
            }
        }
    }
    for(std::pair<sf::Sprite,float> s : engrenages) {
        if(s.first.getGlobalBounds().intersects(rect.getGlobalBounds())) {
            sf::Sprite temp;
            temp = s.first;
            temp.setPosition(temp.getPosition()-topLeft);
            w.draw(temp);
            float rayon = temp.getLocalBounds().width/2;
            sf::Vector2f center = temp.getPosition() + sf::Vector2f{rayon,rayon};
            if( sqrt( pow(center.x-mousePos.x,2) + pow(center.y-mousePos.y,2) ) < rayon ){
                touched = true;
                m_clock.restart();
            }else if( sqrt( pow(center.x-mousePos.x+20,2) + pow(center.y-mousePos.y,2) ) < rayon ){
                touched = true;
                m_clock.restart();
            }else if( sqrt( pow(center.x-mousePos.x,2) + pow(center.y-mousePos.y+20,2) ) < rayon ){
                touched = true;
                m_clock.restart();
            }else if( sqrt( pow(center.x-mousePos.x+20,2) + pow(center.y-mousePos.y+20,2) ) < rayon ){
                touched = true;
                m_clock.restart();
            }
        }
    }
    for(Chip* c : chips) {
        c->render(w,rect.getGlobalBounds());
    }
    return touched;
}
