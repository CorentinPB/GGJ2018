#include "SelectMenu.h"
#include <iomanip> // setprecision
#include <sstream> // stringstream
#include <iostream>

SelectMenu::init() {
    Sprite* s = new Sprite(0,0,"sprites/level_select.png");
    m_sprites.push_back(s);

    m_font.loadFromFile("PoiretOne-Regular.ttf");
    m_text.setFont(m_font);
    m_text.setFillColor(sf::Color::White);
    m_text.setCharacterSize(30);
    m_text.setOrigin(sf::Vector2f(m_text.getLocalBounds().width/2,m_text.getLocalBounds().height/2));


    Button* b = new Button(100,750,"sprites/mainMenuButton.png","sprites/mainMenuButtonPressed.png");
    m_buttons.push_back(b);
    b = new Button(268,308,"sprites/level.png","sprites/levelPressed.png","1");
    m_buttons.push_back(b);
    b = new Button(532,308,"sprites/level.png","sprites/levelPressed.png","2");
    m_buttons.push_back(b);
    b = new Button(796,308,"sprites/level.png","sprites/levelPressed.png","3");
    m_buttons.push_back(b);
    b = new Button(1060,308,"sprites/level.png","sprites/levelPressed.png","4");
    m_buttons.push_back(b);
    b = new Button(1324,308,"sprites/level.png","sprites/levelPressed.png","5");
    m_buttons.push_back(b);
    b = new Button(268,559,"sprites/level.png","sprites/levelPressed.png","6");
    m_buttons.push_back(b);
    b = new Button(532,559,"sprites/level.png","sprites/levelPressed.png","7");
    m_buttons.push_back(b);
    b = new Button(796,559,"sprites/level.png","sprites/levelPressed.png","8");
    m_buttons.push_back(b);
    b = new Button(1060,559,"sprites/level.png","sprites/levelPressed.png","9");
    m_buttons.push_back(b);
    b = new Button(1324,559,"sprites/level.png","sprites/levelPressed.png","10");
    m_buttons.push_back(b);
}

void SelectMenu::draw_to(sf::RenderWindow &window){
    for (Sprite* s : m_sprites) {
        s->draw_to(window);
    }
    int i =0;
    for (Button* b : m_buttons) {
        if(i != 0){
            b->draw_to(window);
            auto it = m_scores.find(i);
            if(it != m_scores.end()){
                sf::Vector2f pos{b->getPosition().x,b->getPosition().y+80};
                m_text.setPosition(pos);
                std::stringstream stream;
                stream << std::fixed << std::setprecision(2) << m_scores[i];
                std::string txt = stream.str();
                m_text.setString("Best time : " + txt);
                m_text.setOrigin(sf::Vector2f(m_text.getGlobalBounds().width/2-50,m_text.getGlobalBounds().height/2-50));
                window.draw(m_text);
            }
        }else{
            b->draw_to(window);
        }
        i++;
    }
}

SelectMenu::~SelectMenu() {
    for(Button* b : m_buttons)
        delete b;
}
