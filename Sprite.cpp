#include "Sprite.h"

Sprite::Sprite(const std::string &file)
    :m_fileName{file}
{
    m_image.loadFromFile(m_fileName);
    m_sprite.setTexture(m_image);
}

Sprite::Sprite(const int &x, const int &y, const std::string &file)
    :m_fileName{file}
{
    m_image.loadFromFile(m_fileName);
    m_image.setSmooth(1);
    m_sprite.setTexture(m_image);
    m_sprite.setPosition({ (float)x, (float)y});
}

Sprite::Sprite(const int &x, const int &y, const std::string &file, const std::string &text)
    :m_fileName{file}
{
    m_with_text = 1;
    m_image.loadFromFile(m_fileName);
    m_image.setSmooth(1);
    m_sprite.setTexture(m_image);
    m_sprite.setPosition({ (float)x, (float)y});
    m_font.loadFromFile("PoiretOne-Regular.ttf");
    m_text.setString(text);
    m_text.setFont(m_font);
    m_text.setColor(sf::Color::White);
    m_text.setCharacterSize(getSize().y/2);
    m_text.setPosition({ (float)x + m_sprite.getGlobalBounds().width/2 - m_text.getGlobalBounds().width/2
                         ,(float)y + m_sprite.getGlobalBounds().height/2 - m_text.getGlobalBounds().height/2
                         + m_text.getPosition().y - m_text.getGlobalBounds().top});
}

sf::Vector2i Sprite::getSize() const {
    return {m_sprite.getTextureRect().width, m_sprite.getTextureRect().height};
}

sf::Vector2f Sprite::getPosition() const {
    return {m_sprite.getPosition()};
}

sf::FloatRect Sprite::getLocalBounds() const {
    return {m_sprite.getLocalBounds()};
}

void Sprite::draw_to(sf::RenderWindow &window) {
    window.draw(m_sprite);
    if (m_with_text)
        window.draw(m_text);
}
