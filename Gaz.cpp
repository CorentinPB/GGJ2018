#include "Gaz.h"

Gaz::Gaz(const sf::Vector2f &mousePos) {
    speed.x = 1.0 * ((rand()%30)-15) / 10.0;
    speed.y = -1.0 * ((rand()%20)+10) / 10.0;
    part.setFillColor(sf::Color::White);
    part.setSize({GAZ_SIZE,GAZ_SIZE});
    part.setPosition(mousePos);
    step = (rand()%20)/10+2;
}

void Gaz::update() {
    alpha -= step;
    part.setFillColor(sf::Color(255,255,255,alpha));
    part.setPosition(part.getPosition() + speed);
}

bool Gaz::isDead() {
    if(alpha <= 0 || part.getPosition().y < GAZ_SIZE)
        return true;
    return false;
}

void Gaz::render(sf::RenderWindow &w) {
    w.draw(part);
}
