#include <MainMenu.h>
#include <iostream>

using namespace std;

void MainMenu::init() {
    Sprite* s = new Sprite(0,0,"sprites/background.png");
    m_sprites.push_back(s);
    s = new Sprite(1405,716,"sprites/GGJ.png");
    m_sprites.push_back(s);

    Button* b = new Button(594,398,"sprites/playButton.png","sprites/playButtonPressed.png");
    m_buttons.push_back(b);
    b = new Button(594,608,"sprites/exitButton.png","sprites/exitButtonPressed.png");
    m_buttons.push_back(b);
    b = new Button(100,750,"sprites/credit_icon.png","sprites/credit_icon.png");
    m_buttons.push_back(b);

    trajectoires[0] = {170.0, 394.0};
    trajectoires[1] = {423.0, 394.0};
    trajectoires[2] = {423.0, 814.0};
    trajectoires[3] = {1146.0, 814.0};
    trajectoires[4] = {1146.0, 394.0};
    trajectoires[5] = {1429.0, 394.0}                                ;
    trajectoires[6] = {1146.0, 394.0};
    trajectoires[7] = {1146.0, 814.0};
    trajectoires[8] = {423.0, 814.0};
    trajectoires[9] = {423.0, 394.0};

    data = sf::RectangleShape();
    data.setFillColor(sf::Color::Black);
    data.setSize({25,25});
    dataPos = 0;
    data.setPosition(trajectoires[dataPos]);
    update();
}

MainMenu::~MainMenu() {
    for(Button* b : m_buttons)
        delete b;
}

void MainMenu::update() {
    if( abs(data.getPosition().x-trajectoires[dataPos%10].x) < 0.1
            && abs(data.getPosition().y-trajectoires[dataPos%10].y) < 0.1 ) {
        data.setPosition(trajectoires[dataPos%10]);
        sf::Vector2f temp = trajectoires[(dataPos+1)%10] - trajectoires[(dataPos)%10];
        dataVelocity = {temp.x/144.0,temp.y/144.0};
        ++dataPos;
    }
    else {
        data.setPosition(data.getPosition() + dataVelocity);
    }

}

void MainMenu::draw_to(sf::RenderWindow &window) {
    Menu::draw_to(window);
    window.draw(data);
}
