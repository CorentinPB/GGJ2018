#ifndef MAINMENU_H
#define MAINMENU_H

#include "Menu.h"

class MainMenu : public Menu {
public:
    void init();
    ~MainMenu();
    void update();
    void draw_to(sf::RenderWindow &window);
private:
    sf::RectangleShape data;
    int dataPos;
    sf::Vector2f dataVelocity;
    std::array<sf::Vector2f,10> trajectoires;

};

#endif // MAINMENU_H
