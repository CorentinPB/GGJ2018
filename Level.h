#ifndef LEVEL_H
#define LEVEL_H

#include <array>
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Chip.h"
#include "SelectMenu.h"
#include <map>

const int LEFT_PART = 0;
const int RIGHT_PART = 0;

float constraint(float val, float min, float max);

class Level
{
public:
    Level(SelectMenu* sm);
    int nbLvl;
    void updateChips();
    void update(sf::Vector2f &mouse);
    std::string getTime();
    float getTimeFloat();
    bool isWin();
    bool render(sf::RenderWindow &w, const sf::Vector2f &mousePos, sf::FloatRect cursor);
    sf::Vector2f toScreen(const sf::Vector2f &pos);
    sf::Vector2f getStartPos();
    void init(int nb);
private:
    SelectMenu* selectmenu;
    const static int TailleXGrille = 40;
    const static int TailleYGrille = 25;
    int realX;
    int realY;
    float TailleCase = 200.0;
    std::array<std::array<int,TailleYGrille>,TailleXGrille> grille;
    std::vector<Chip*> chips;
    std::vector<sf::RectangleShape> rectangles;
    std::vector<std::pair<sf::Sprite,float>> engrenages;
    sf::Texture textureEngrenage;
    sf::Vector2i startPosition;
    sf::Vector2i endPosition;
    sf::Clock m_clock;
    bool m_isWin;
    float endTime;
};

#endif // LEVEL_H
