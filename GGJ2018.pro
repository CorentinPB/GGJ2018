TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11 -pedantic -Wall -Wextra

LIBS += -L C:/SFML/lib

CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-main -lsfml-network -lsfml-window -lsfml-system
CONFIG(debug, debug|release): LIBS += -lsfml-audio-d -lsfml-graphics-d -lsfml-main-d -lsfml-network-d -lsfml-window-d -lsfml-system-d

INCLUDEPATH += C:/SFML/include
DEPENDPATH += C:/SFML/include

SOURCES += \
    Appli.cpp \
    Level.cpp \
    Menu.cpp \
    Main.cpp \
    GameMenu.cpp \
    Sprite.cpp \
    MainMenu.cpp \
    Button.cpp \
    SelectMenu.cpp \
    Gaz.cpp \
    Chip.cpp \
    creditmenu.cpp

HEADERS += \
    Appli.h \
    Level.h \
    Menu.h \
    GameMenu.h \
    Sprite.h \
    MainMenu.h \
    Button.h \
    SelectMenu.h \
    Gaz.h \
    Chip.h \
    creditmenu.h
