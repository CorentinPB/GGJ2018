#ifndef MENU_H
#define MENU_H

#include "SFML/Graphics.hpp"
#include <vector>
#include "Sprite.h"
#include "Button.h"
#include <iostream>

class Menu
{
public:
    ~Menu();

    std::vector<Sprite*>    m_sprites;
    std::vector<Button*>    m_buttons;

    void add_sprite(const std::string &file, const int &x, const int &y);

    void draw_to(sf::RenderWindow &window);
    int buttonContainsIndice(const sf::Vector2f &mouse) const;
    int containsIndice(int i,const sf::Vector2f &mouse) const;
    Button* buttonContains(const sf::Vector2f &mouse) const;
};

#endif // MENU_H
