#include "Button.h"

Button::Button(const std::string &file)
    :m_isPressed{false}
{
    m_sprite = new Sprite(file);
}

Button::Button(const std::string &file, const std::string &filePressed)
    :m_isPressed{false}
{
    m_sprite = new Sprite(file);
    m_spritePressed = new Sprite(filePressed);
}

Button::Button(const int &x, const int &y, const std::string &file)
    :m_isPressed{false}
{
    m_sprite = new Sprite(x,y,file);
}

Button::Button(const int &x, const int &y, const std::string &file,  const std::string &filePressed)
    :m_isPressed{false}
{
    m_sprite = new Sprite(x,y,file);
    m_spritePressed = new Sprite(x,y,filePressed);
}

sf::Vector2f Button::getPosition(){
    return m_sprite->getPosition();
}

Button::Button(const int &x, const int &y, const std::string &file, const std::string &filePressed, const std::string &text)
    :m_isPressed{false}
{
    m_sprite = new Sprite(x,y,file,text);
    m_spritePressed = new Sprite(x,y,filePressed,text);
}

bool Button::contains(const sf::Vector2f &vect) const {
    return ( (vect.x >= m_sprite->getPosition().x)
             && (vect.x <= m_sprite->getPosition().x+m_sprite->getLocalBounds().width)
             && (vect.y >= m_sprite->getPosition().y)
             && (vect.y <= m_sprite->getPosition().y+m_sprite->getLocalBounds().height) );
}

void Button::setPressed(bool b){
    m_isPressed = b;
}

void Button::draw_to(sf::RenderWindow &window) {
    if(!m_isPressed)
        m_sprite->draw_to(window);
    else
        m_spritePressed->draw_to(window);
}
