#ifndef APPLI_H
#define APPLI_H

#include "MainMenu.h"
#include "SelectMenu.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <SFML/Audio.hpp>
#include <stdio.h>
#include <math.h>
#include "Level.h"
#include "GameMenu.h"
#include "Gaz.h"
#include "creditmenu.h"

const int WIDTH = 1600;
const int HEIGHT = 900;

class Appli
{
public:
    // Constructor
    Appli();
    // Destructor
    ~Appli();
    // Function
    void run();

private:
    SelectMenu selectMenu;
    Level level;
    std::vector<Gaz> particles;

    sf::Text m_time;
    sf::Font m_font;

    sf::RectangleShape  m_cursor;
    enum class Place {MAIN_MENU, CREDITS, IN_GAME, SELECT_MENU, END_MENU} m_place;

    bool m_appli_running;

    sf::Music musicMenu;
    sf::Music musicGame;

    MainMenu mainMenu;
    CreditMenu creditmenu;
    sf::Vector2i old_mousePos;
    GameMenu gameMenu;
    Button* m_selected_button;
    int m_selected_button_int;

    sf::RenderWindow    m_window;           // Application window

    sf::Vector2f        m_mouse;            // mouse position
    sf::Vector2f        m_mousePos;            // mouse position

    //Functions

    void draw();        // draw
    void update();      // update

    void keyPressed(sf::Keyboard::Key code);
    void KeyReleased(sf::Keyboard::Key code);
    void mousePressed();
    void mouseReleased();
    void mouseMoved();

    void process_events();

};

#endif // APPLI_H
