#ifndef GAZ_H
#define GAZ_H

#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

const int GAZ_SIZE = 4;

class Gaz
{
public:
    Gaz(const sf::Vector2f &mousePos);
    void render(sf::RenderWindow &w);
    void update();
    bool isDead();
private:
    sf::RectangleShape part;
    sf::Vector2f speed;
    float alpha = 255.0;
    float step;
};

#endif // GAZ_H
