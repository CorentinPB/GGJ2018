#include "Menu.h"

Menu::~Menu() {
    for(Button* b : m_buttons)
        delete b;
    for(Sprite* s : m_sprites)
        delete s;
}

void Menu::add_sprite(const std::string &file, const int &x, const int &y) {
    Sprite* sprite = new Sprite(x,y,file);
    m_sprites.push_back(sprite);
}

void Menu::draw_to(sf::RenderWindow &window) {
    for (Sprite* s : m_sprites) {
        s->draw_to(window);
    }
    for (Button* b : m_buttons) {
        b->draw_to(window);
    }
}

int Menu::buttonContainsIndice(const sf::Vector2f &mouse) const {
    for(unsigned i=0 ; i< m_buttons.size() ; i++)
        if(m_buttons[i]->contains(mouse) == 1)
            return i;
    return -1;
}


Button* Menu::buttonContains(const sf::Vector2f &mouse) const {
    for(unsigned i=0 ; i< m_buttons.size() ; i++)
        if(m_buttons[i]->contains(mouse) == 1)
            return m_buttons[i];
    return nullptr;
}

int Menu::containsIndice(int i,const sf::Vector2f &mouse) const {
    return m_buttons[i]->contains(mouse) == 1;
}
